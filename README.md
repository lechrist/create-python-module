# How-create-python-module

# First create a new project that will contain this following files

    myproject/
        README.md
        LICENCE
        CHANGELOG.txt
        setup.py
        mymodule/
            __init__.py

# Now in setup.py add following codes:

    `"""
    Your licence goes here
    """
    
    from setuptools import setup, find_packages
 
    # See note below for more information about classifiers
    def readme():
    with open('README.md') as f:
        return f.read()

    setup(
    name='mymodule',
    version='0.0.1',
    description='Demo project',
    long_description=readme(),
    long_description_content_type='text/markdown',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent',
        'Intended Audience :: Developers',
    ],
    url='https://github.com/CHRISTMardochee/django-module',  # the URL of your package's home page e.g. github link
    author='MothinelKG',
    author_email='kgmardochee@gmail.com',
    keywords='core package', # used when people are searching for a module, keywords separated with a space
    license='MIT', # note the American spelling
    packages=['mymodule'],
    install_requires=find_packages(), # a list of other Python modules which this module depends on.  For example RPi.GPIO
    include_package_data=True,
    zip_safe=False
    )`

# Create Virtual Environment
    python3 -m venv env

# Activate Virtual Environment
  ## On linux:
    source env/bin/activate
  ## On Windows:
    source env\Scripts\activate

# Test your module locally

### Create file example.py in your module **mymodule**  and add following codes below:  
    def say_hello(name):
        """ Says hello to given name """
        print(f'Hello  {name}')

### Install your module locally to test in python command shell:
    pip install -e /myproject/mymodule

### Run the Python interpreter (make sure you’re still in your virtualenv)
    python3

### and from the interpreter shell import the package (You should have somethimg like this):
    >>> from mymodule.example import say_hello
    >>> say_hello("Your Name")
    'Hello Your Name'(Your must have this)
    >>>

# Generating distribution archives

### Install build package

#### Make sure you have the latest versions of PyPA’s build installed:
    python3 -m pip install --upgrade build

#### Build your module:
    python3 -m build
#### This command should output a lot of text and once completed should generate two files in the dist directory:*
    dist/
        example_pkg_YOUR_USERNAME_HERE-0.0.1-py3-none-any.whl
        example_pkg_YOUR_USERNAME_HERE-0.0.1.tar.gz

# Uploading the distribution archives located in your  dist directory

### You’ll need to install **Twine** to upload the distribution packages
    <!-- python3 -m pip install --user --upgrade twine -->
    py -m twine upload --repository-url https://upload.pypi.org/legacy/ dist/*


### Deploy your module
    python3 -m twine upload --repository testpypi dist/*
    You will be prompted for a username and password. For the username, use __token__. 
    For the password, use the token value, including the pypi- prefix.

### After the command completes, you should see output similar to this:
    Enter your username: [your username]
    Enter your password:
    Uploading example_pkg_YOUR_USERNAME_HERE-0.0.1-py3-none-any.whl
    100%|█████████████████████| 4.65k/4.65k [00:01<00:00, 2.88kB/s]
    Uploading example_pkg_YOUR_USERNAME_HERE-0.0.1.tar.gz
    100%|█████████████████████| 4.25k/4.25k [00:01<00:00, 3.05kB/s]

### Installing your newly uploaded package
    pip install mymodule==0.0.1
    
### pip should install the package the output should look something like this:

    Collecting mymodule==0.0.1
    Downloading https://files.pythonhosted.org/packages/9e/d4/b8b99c8fe9bf8d7eb630fd74afff054f5302cb7a296963280d2d899d08eb/mymodule-0.0.1-py3-none-any.whl
    Collecting PyYAML (from mymodule==0.0.1)
    Using cached https://files.pythonhosted.org/packages/97/d3/24097209f6af04fcdbb40500480a0feaa62164e60bca4c9532f0e9354e47/PyYAML-5.4.1-cp38-cp38-win_amd64.whl
    Installing collected packages: PyYAML, mymodule
    Successfully installed PyYAML-5.4.1 mymodule-0.0.1

### You can test that it was installed correctly by importing the package. Run the Python interpreter (make sure you’re still in your virtualenv):
    python3
### and from the interpreter shell import the package:
    >>> import mymodule

**Congratulations, you’ve packaged and distributed a Python module! ✨ 🍰 ✨**



# Sources
### [https://www.youtube.com/watch?v=uVJf5wuPpyw](https://www.youtube.com/watch?v=uVJf5wuPpyw)
### [https://packaging.python.org/tutorials/packaging-projects/](https://packaging.python.org/tutorials/packaging-projects/)
### [https://pypi.org/project/twine/](https://pypi.org/project/twine/)
### [https://packaging.python.org/guides/distributing-packages-using-setuptools/#create-an-account](https://packaging.python.org/guides/distributing-packages-using-setuptools/#create-an-account)
### [https://pypi.org/help/#apitoken](https://pypi.org/help/#apitoken)
### [https://packaging.python.org/specifications/pypirc/#pypirc](https://packaging.python.org/specifications/pypirc/#pypirc)





> By Mardochee KIKIGBGBAN





